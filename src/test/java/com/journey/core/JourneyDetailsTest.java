package com.journey.core;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class JourneyDetailsTest {

	private JourneyDetails journeydetails;
	private Commuter commuter;
	private Vacationer vacationer;
	private PassengersList passengerslist;
	private Passenger p;



	List<PassengersList> passengerList;
	@Before
	public void init()
	{
		journeydetails= new JourneyDetails();
		commuter= new Commuter();
		vacationer= new Vacationer();
		passengerList= new ArrayList<PassengersList>();
		//(String passengerType,String passengerName,int noOfstops,int miles,boolean newspaper,boolean meals,boolean frequentRidercard)
		passengerList.add(new PassengersList("Commuter","Monica",3,0,false,false,true));
		passengerList.add(new PassengersList("Commuter","Pavan",5,0,false,false,true));
		passengerList.add(new PassengersList("Commuter","Madhavi",4,0,true,false,false));
		passengerList.add(new PassengersList("Vacationer","Mohan",0,90,true,true,false));
		passengerList.add(new PassengersList("Vacationer","Kaparthi",0,199,true,true,false));

	}


	@Test

	public void noOfMealsTest()

	{
		int actual=journeydetails.totalnoOfMeals(passengerList);
		int expected= 3;

		Assert.assertEquals(expected, actual);


	}
	@Test
	public void noOfNewsPaperCountTest()

	{
		int actual= journeydetails.totalnoOfNewsPapers(passengerList);
		int expected=3;

		Assert.assertEquals(expected, actual);

	}

	@Test

	public void totalFareChargesTest()
	{ 

		double actual= journeydetails.totalFareCharges(passengerList);
		double expected= 150;

		Assert.assertEquals(expected, actual,0.1);

	}

}
