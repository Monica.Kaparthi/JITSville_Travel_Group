package com.journey.core;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import junit.framework.Assert;

public class VacationerTest {
private Vacationer vacationer;
	
	private PassengersList passengerlist;
	
	@Before
	
	public void init()
	{
		vacationer= new Vacationer();	
		
	}
	
	@Test
	
	public void calculateFareCostTest()
	{
		//(String passengerType,String passengerName,int noOfstops,int miles,boolean newspaper,boolean meals,boolean frequentRidercard)
		
		double actual= vacationer.fareCharges(new PassengersList("Vacationer","Monica",0,41,false,false,false));
		double expected= 20.5;
		Assert.assertEquals(expected, actual,0.01);
		
	}
	
	@Rule public ExpectedException thrown= ExpectedException.none();
	@Test
	
	public void checkForMilesmoreThan4000()
	
	{
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Vacationer should not travel for less than 5 or more than 4000 miles");
		double actual= vacationer.fareCharges(new PassengersList("Vacationer","Monica",0,1,false,false,false));
		
	}
	
	@Test
	public void noOfMealsTest()
	{
		int actual= vacationer.noOfMeals(new PassengersList("Vacationer","Monica",0,101,false,true,false));
		int expected=2;
		
		Assert.assertEquals(expected, actual);
		
		
		
	}
	
	


}
