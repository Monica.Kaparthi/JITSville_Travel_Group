package com.journey.core;

import org.junit.Assert.*;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CommuterTest {
	
	private Commuter commuter;
	
	private PassengersList passengerlist;
	
	@Before
	
	public void init()
	{
		commuter= new Commuter();	
		
	}
	
	@Test
	
	public void calculateFareCostFreqRiderTest()
	{
		
		//(String passengerType,String passengerName,int noOfstops,int miles,boolean newspaper,boolean meals,boolean frequentRidercard)
		
		double actual= commuter.fareCharges(new PassengersList("Commuter","Monica",3,0,false,false,true));
		double expected= 1.35;
		Assert.assertEquals(expected, actual,0.01);
	}
@Test
	
	public void calculateFareCostNoFreqRiderTest()
	{
		
		//(String passengerType,String passengerName,int noOfstops,int miles,boolean newspaper,boolean meals,boolean frequentRidercard)
		
		double actual= commuter.fareCharges(new PassengersList("Commuter","Monica",3,0,false,false,false));
		double expected= 1.5;
		Assert.assertEquals(expected, actual,0.01);
	}
	


}
