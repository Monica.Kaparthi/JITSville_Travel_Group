package com.journey.core;

import java.util.List;

public class JourneyDetails {

//This class calculates total no. of newspapers, total number of meals and total fare charges.
	PassengerFactory factory= new PassengerFactory();
	Passenger p;
	Vacationer vacationer = new Vacationer();
	public int totalnoOfMeals(List<PassengersList> passengerslist) {
		int mealcount=0;

		for(PassengersList passengers : passengerslist)
		{
			if("Vacationer".equalsIgnoreCase(passengers.getPassengerType()))
			{

				mealcount+=vacationer.noOfMeals(passengers);
			}
		}


		return mealcount;
	}

	public int totalnoOfNewsPapers(List<PassengersList> passengerslist) {

		int newpapercount=0;

		for(PassengersList passengers : passengerslist)
		{
			if(passengers.isNewspaper())

			{
				newpapercount++;

			}
		}


		return newpapercount;
	}

	public double totalFareCharges(List<PassengersList> passengerslist) {
	
		double totalFareCost=0;
		
		for(PassengersList passengers : passengerslist )
		{
			
			p=factory.getPassengerType(passengers);
			totalFareCost+=p.fareCharges(passengers);
			
			
			
		}
		return totalFareCost;
	}






}
