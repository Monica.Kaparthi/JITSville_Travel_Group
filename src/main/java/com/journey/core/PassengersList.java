package com.journey.core;

public class PassengersList {
	
	//Bean Class
	private String passengerType;
	private String passengerName;
	private int noOfstops;
	private int miles;
	private boolean newspaper;
	private boolean meals;
	private boolean frequentRidercard;
	
	
	public PassengersList(String passengerType,String passengerName,int noOfstops,int miles,boolean newspaper,boolean meals,boolean frequentRidercard)
	{
		this.passengerType= passengerType;
		this.passengerName=passengerName;
		this.noOfstops= noOfstops;
		this.miles= miles;
		this.newspaper=newspaper;
		this.meals= meals;
		this.frequentRidercard= frequentRidercard;
		
	}
	
	public String getPassengerType() {
		return passengerType;
	}
	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}
	
	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
	
	
	public int getNoOfstops() {
		return noOfstops;
	}
	
	public int getMiles() {
		return miles;
	}
	
	
	public boolean isNewspaper() {
		return newspaper;
	}
	public void setNewspaper(boolean newspaper) {
		this.newspaper = newspaper;
	}
	public boolean isMeals() {
		return meals;
	}
	public void setMeals(boolean meals) {
		this.meals = meals;
	}
	public boolean isFrequentRidercard() {
		return frequentRidercard;
	}
	public void setFrequentRidercard(boolean frequentRidercard) {
		this.frequentRidercard = frequentRidercard;
	}
	//Displays all the bean values in string format

	public String toString()
	{
		
		return("Bean[PassengerType="+passengerType+", passengerName="+passengerName+ ", noOfStops="+ noOfstops+", miles="+miles+", newspaper=" +newspaper+", meals="+meals+
				", frequentRidercard=" +frequentRidercard);
	}
	 
	

}
