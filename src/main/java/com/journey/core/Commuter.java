package com.journey.core;

public class Commuter implements Passenger {
	
	//calculating fare charges for commuter.

	public double fareCharges(PassengersList passengers) {

		int noOfstops= passengers.getNoOfstops();
		double cost=0;

		cost= (noOfstops*Rate_Factor);
		if(passengers.isFrequentRidercard())
		{
			cost-=cost*0.1;

		}

		return cost;
	}

}
