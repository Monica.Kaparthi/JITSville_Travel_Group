package com.journey.core;

public class Vacationer implements Passenger,Meals{

double cost=0;


//Calculating Fare Charges for Vacationer.
	public double fareCharges(PassengersList passengers) {
		
		int noOfMiles= passengers.getMiles();
		
		
		
		if(noOfMiles<5 || noOfMiles>4000)
			
		{
			throw new IllegalArgumentException("Vacationer should not travel for less than 5 or more than 4000 miles");
			
		}
		else
		{
			cost= noOfMiles*Rate_Factor;
		
		}
		
		return cost;
	}

	//Calculate number of meals per passenger.

	public int noOfMeals(PassengersList passenger) {
		
		int noOfmeals=0;
		int miles=passenger.getMiles();
		
		if(passenger.isMeals()) {
			noOfmeals=miles/100;
		if(miles%100<100 && miles%100>0)
		{
			noOfmeals++;
			
		}
		
		}
		return noOfmeals;
		
	}
	
	

	
	

}
