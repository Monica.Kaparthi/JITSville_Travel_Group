package com.journey.core;

public class PassengerFactory {
	
	
	Passenger passenger;
	
	public Passenger getPassengerType (PassengersList passengerlist)
	{
		if(passengerlist.getPassengerType().equalsIgnoreCase("Commuter"))
		{
			passenger= new Commuter();
			
		}
		
		else if(passengerlist.getPassengerType().equalsIgnoreCase("Vacationer"))
			
		{
			passenger=new Vacationer();
			
		}
		
		else
			
		{
			throw new IllegalArgumentException("Passenger Type can only be Commuter or Vacationer");
			
		}
		
		
		return passenger;
		
		
	}

}
